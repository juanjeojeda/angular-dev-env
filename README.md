# Description

Project to create a simple developer enviroment and workflow for developing with Angular and based on Docker.

## Angular CLI development container

This is a minimal image with **Node.js** and **Angular CLI** for local development. It aims to fix two issues:

* Avoid having to install any dev dependencies at the local machine.
* Be sure everyone at the team has the same tools and libs.
* Be able to create and test Angular project quick and easily.
* Be able to run production like environment locally.
* Avoid having to mess too much with Docker.
* Avoid typical permissions errors with when use docker with local files.

## Important

This image is very opinionated about the *user* and *group* ID. I made this for my teammates at [Edosoft](https://edosoft.es/) and at our workstations the user has `UID=1001` and `GUID=1001`, so that is what I used. But you can rebuild the image using your `UID` and `GUI`. 

## Dockerfile

Here you have the used `Dockerfile`:
[Dockerfile.base](https://gitlab.com/juanje.ojeda.edosoft/angular-dev-env/blob/master/Dockerfile.base)

You can build your own image by running this command:
```
$ docker build . -f Dockerfile.base -t your_image_name
```

You can customize some options, like the `UID` and `GUID`, by passing them as  `--build-arg `:
```
$  docker build --build-arg UID=1000  --build-arg GUID=1000 . -f Dockerfile.base -t your_image_name
```

This is useful if your normal user has another `UID` and `GUID`.

## Software versions

* **Docker image base:** `node:13.2.0-alpine`
* **Node.js:** `13.2.0`
* **Angular:** `8.2.14`
* **Angular CLI:** `8.3.20`

## Workflow with the image

Create a `docker-compose.yml` like this one:
```dockerfile
version: '3'

services:
  ng:
    image: juanje/angular-cli
    container_name: angular-cli
    volumes:
      - '.:/app'
    ports:
      - '4200:4200'
  npm:
    image: juanje/angular-cli
    container_name: npm
    volumes:
      - '.:/app'
    entrypoint: 'npm'
  test:
    image: juanje/angular-cli-chrome
    container_name: angular-cli-testing
    volumes:
      - '.:/app'
    entrypoint: 'ng test --no-watch --no-progress'
```

Now you can run commands like these:
* Create a new Angular project:
```
$ docker-compose run ng new projet_name
```

* Serve the project at localhost to make changes and see the changes at your browser:
```
$ docker-compose run --service-ports ng serve --host=0.0.0.0
```

> This will serve your project at http://localhost:4200/.


* Create new [component|service|module|etc] (i.e. `ng generate component mycomponent`):
```
$ docker-compose run ng generate module module_name
$ docker-compose run ng generate component component_name
$ docker-compose run ng generate component module_name/component_name
$ docker-compose run ng generate service service_name
```

* Build static files to deploy elsewhere:
```
$ docker-compose run ng build --prod
```

* Install dependencies from `package.json`:
```
$ docker-compose run npm install
```

* Run the test suites:
```
$ docker-compose run test
```

## License and Author

Author:: Juanje Ojeda (<juanje.ojeda@edosoft.es>)

Copyright:: 2019, Juanje Ojeda

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
