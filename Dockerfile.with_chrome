# Minimal Node image to run Angular commands.
FROM node:13.2.0-alpine

LABEL maintainer="juanje.ojeda@edosoft.es"
LABEL name="angular-cli-chrome"
LABEL version="0.1.0"

# Software version. It can be passed by --build-arg on build time.
ARG NODE_VERSION="13.2.0"
ARG ANGULAR_VERSION="8.2.14"
ARG ANGULAR_CLI_VERSION="8.3.20"
LABEL version.node=${NODE_VERSION}
LABEL version.angular=${ANGULAR_VERSION}
LABEL version.angular_cli=${ANGULAR_CLI_VERSION}

# Install Angular.
RUN npm install -g @angular/cli

# Change the UID and GUID for the existing 'node' user.
# This is to match the tipical dev user at Edosoft.
ARG UID=1001
ARG GUID=1001
RUN apk --no-cache add shadow && \
    groupmod -g ${GUID} node && \
    usermod -u ${UID} node

# Run 'ng' as the user 'node'.
WORKDIR /app
RUN chown node:node /app

# Installs latest Chromium package.
RUN apk add --no-cache \
    libstdc++ \
    chromium \
    harfbuzz \
    nss \
    freetype \
    ttf-freefont

USER node
ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

# Start  the comand 'ng'.
ENTRYPOINT ["ng"]